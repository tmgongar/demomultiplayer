﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Photon.Pun;


namespace Com.Chicutrino.FPSMultiplayerWelton
{
    public class Player : MonoBehaviourPunCallbacks, IPunObservable
    {
        #region Variables

        public float speed, sprintModifier, crouchModifier, slideModifier, jumpForce, lengthOfSlide, slideAmount, crouchAmount;
        public int maxHealth;
        public Camera normalCam, weaponCam;
        public GameObject cameraParent, standingCollider, crouchingCollider;
        public Transform groundDetector, weaponParent;
        public LayerMask ground;

        private Transform uiHealthBar;
        private Text uiAmmo;

        private Rigidbody rig;
        private Vector3 weaponParentOrigin, weaponParentCurrentPos, targetWeaponBobPosition, slideDir, origin;
        private float baseFOV, sprintFOVModifier = 1.25f, movementCounter, idleCounter, slideTime, aimAngle;
        private int currentHealth;

        private Manager manager;
        private Weapon weapon;

        private bool crouched, sliding, isAiming;


        #endregion

        #region Photon Callbacks

        public void OnPhotonSerializeView(PhotonStream _stream, PhotonMessageInfo _message) //Un tipo de string que puedes ir grabando sobre los datos que queramos
        {
            if (_stream.IsWriting)
            {
                _stream.SendNext((int)(weaponParent.transform.localEulerAngles.x * 100f)); //Guardamos el ángulo de apuntado en entero para optimizar espacio
            }
            else
            {
                aimAngle = (int)_stream.ReceiveNext() / 100f;
            }
        }

        #endregion

        #region Monobehaviour Callbacks

        void Start()
        {
            manager = GameObject.Find("Manager").GetComponent<Manager>();
            weapon = GetComponent<Weapon>();
            currentHealth = maxHealth;

            cameraParent.SetActive(photonView.IsMine); //Comprobamos que la camara es nuestra y la activamos
            if (!photonView.IsMine)
            {
                gameObject.layer = 11; //Checkeamos otros jugadores y le asignamos capa Player
                standingCollider.layer = 11;
                crouchingCollider.layer = 11;
            }

            baseFOV = normalCam.fieldOfView;
            origin = normalCam.transform.localPosition;

            if(Camera.main) Camera.main.enabled = false;

            rig = GetComponent<Rigidbody>();
            weaponParentOrigin = weaponParent.localPosition;
            weaponParentCurrentPos = weaponParentOrigin;

            if (photonView.IsMine)
            {
                uiHealthBar = GameObject.Find("HUD/Health/Bar").transform;
                uiAmmo = GameObject.Find("HUD/Ammo/Text").GetComponent<Text>();
                RefreshHealthBar();
            }

        }

        void Update()
        {
            if (!photonView.IsMine)
            {
                RefreshMultiplayerState();
                return;
            }

            //Axis
            float tHMove = Input.GetAxisRaw("Horizontal");
            float tVMove = Input.GetAxisRaw("Vertical");

            //Controls
            bool sprint = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
            bool jump = Input.GetKeyDown(KeyCode.Space);
            bool crouch = Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl);
            bool pause = Input.GetKeyDown(KeyCode.Escape);

            //States
            bool isGrounded = Physics.Raycast(groundDetector.position, Vector3.down, 0.15f, ground);
            bool isJumping = jump && isGrounded;
            bool isSprinting = sprint && tVMove > 0 && !isJumping && isGrounded;
            bool isCrouching = crouch && !isSprinting && !isJumping && isGrounded;

            //Pause
            if (pause)
            {
                GameObject.Find("Pause").GetComponent<Pause>().TogglePause();
            }

            if (Pause.paused)
            {
                tHMove = 0f;
                tVMove = 0f;
                sprint = false;
                jump = false;
                crouch = false;
                pause = false;
                isGrounded = false;
                isJumping = false;
                isSprinting = false;
                isCrouching = false;
            }

            //Crouching
            if (isCrouching)
            {
                photonView.RPC("SetCrouch", RpcTarget.All, !crouched);
            }

            //Jumping
            if (isJumping)
            {
                if(crouched) photonView.RPC("SetCrouch", RpcTarget.All, false);
                rig.AddForce(Vector3.up * jumpForce);
            }

            //Respawn all players
            if (Input.GetKeyDown(KeyCode.U)) TakeDamage(100);

            //Head Bob
            if(sliding)
            {
                //sliding
                HeadBob(movementCounter, .15f, .075f);
                weaponParent.localPosition = Vector3.Lerp(weaponParent.localPosition, targetWeaponBobPosition, Time.deltaTime * 10f);
            }
            else if (tHMove == 0 && tVMove == 0)
            {
                //idling
                HeadBob(idleCounter, .01f, .01f);
                idleCounter += Time.deltaTime;
                weaponParent.localPosition = Vector3.Lerp(weaponParent.localPosition, targetWeaponBobPosition, Time.deltaTime * 2f);
            }
            else if(!isSprinting && !crouched)
            {
                //walking
                HeadBob(movementCounter, .035f, .035f);
                movementCounter += Time.deltaTime * 3f;
                weaponParent.localPosition = Vector3.Lerp(weaponParent.localPosition, targetWeaponBobPosition, Time.deltaTime * 6f);
            }
            else if(crouched)
            {
                //crouching
                HeadBob(movementCounter, .02f, .02f);
                movementCounter += Time.deltaTime * 1.75f;
                weaponParent.localPosition = Vector3.Lerp(weaponParent.localPosition, targetWeaponBobPosition, Time.deltaTime * 6f);
            }
            else
            {
                //sprinting
                HeadBob(movementCounter, .15f, .075f);
                movementCounter += Time.deltaTime * 7f;
                weaponParent.localPosition = Vector3.Lerp(weaponParent.localPosition, targetWeaponBobPosition, Time.deltaTime * 10f);
            }

            //UI Refreshes
            RefreshHealthBar();
            weapon.RefreshAmmo(uiAmmo);

        }

        void FixedUpdate()
        {
            if (!photonView.IsMine) return;

            //Axis
            float tHMove = Input.GetAxisRaw("Horizontal");
            float tVMove = Input.GetAxisRaw("Vertical");

            //Controls
            bool sprint = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
            bool jump = Input.GetKeyDown(KeyCode.Space);
            bool slide = Input.GetKey(KeyCode.LeftControl);
            bool aim = Input.GetMouseButton(1);

            //States
            bool isGrounded = Physics.Raycast(groundDetector.position, Vector3.down, 0.1f, ground);
            bool isJumping = jump && isGrounded;
            bool isSprinting = sprint && tVMove > 0 && !isJumping && isGrounded;
            bool isSliding = isSprinting && slide && !sliding;
            isAiming = aim && !isSliding && !isSprinting;

            //Pause
            if (Pause.paused)
            {
                tHMove = 0f;
                tVMove = 0f;
                sprint = false;
                jump = false;
                isGrounded = false;
                isJumping = false;
                isSprinting = false;
                isSliding = false;
                isAiming = false;
            }


            //Jumping
            if (isJumping)
            {
                rig.AddForce(Vector3.up * jumpForce);
            }

            //Movements
            Vector3 tDirection = Vector3.zero;
            float tAdjustedSpeed = speed;

            if (!sliding)
            {
                tDirection = new Vector3(tHMove, 0, tVMove);
                tDirection.Normalize();
                tDirection = transform.TransformDirection(tDirection);

                if (isSprinting)
                {
                    if (crouched) photonView.RPC("SetCrouch", RpcTarget.All, false);
                    tAdjustedSpeed *= sprintModifier;
                }
                else if (crouched)
                {
                    tAdjustedSpeed *= crouchModifier;
                }
            }
            else
            {
                tDirection = slideDir;
                tAdjustedSpeed *= slideModifier;
                slideTime -= Time.deltaTime;
                if(slideTime <= 0)
                {
                    sliding = false;
                    weaponParentCurrentPos -= Vector3.up * 0.5f;
                }
            }

            Vector3 tTargetVelocity = tDirection * tAdjustedSpeed * Time.deltaTime;
            tTargetVelocity.y = rig.velocity.y;
            rig.velocity = tTargetVelocity;

            //Sliding
            if (isSliding)
            {
                sliding = true;
                slideDir = tDirection;
                slideTime = lengthOfSlide;
                weaponParentCurrentPos += Vector3.down * (slideAmount - crouchAmount);
                if (!crouched) photonView.RPC("SetCrouch", RpcTarget.All, true);
            }

            //Aiming
            weapon.Aim(isAiming);

            //Camera Stuff
            if (sliding)
            {
                normalCam.fieldOfView = Mathf.Lerp(normalCam.fieldOfView, baseFOV * sprintFOVModifier * 1.15f, Time.deltaTime * 8f);
                weaponCam.fieldOfView = Mathf.Lerp(weaponCam.fieldOfView, baseFOV * sprintFOVModifier * 1.15f, Time.deltaTime * 8f);

                normalCam.transform.localPosition = Vector3.Lerp(normalCam.transform.localPosition, origin + Vector3.down * slideAmount, Time.deltaTime * 6f);
                weaponCam.transform.localPosition = Vector3.Lerp(weaponCam.transform.localPosition, origin + Vector3.down * slideAmount, Time.deltaTime * 6f);
            }
            else
            {
                if (isSprinting)
                {
                    normalCam.fieldOfView = Mathf.Lerp(normalCam.fieldOfView, baseFOV * sprintFOVModifier, Time.deltaTime * 8f);
                    weaponCam.fieldOfView = Mathf.Lerp(weaponCam.fieldOfView, baseFOV * sprintFOVModifier, Time.deltaTime * 8f);
                }
                else if (isAiming)
                {
                    normalCam.fieldOfView = Mathf.Lerp(normalCam.fieldOfView, baseFOV * weapon.currentGunData.mainFOV, Time.deltaTime * 8f);
                    weaponCam.fieldOfView = Mathf.Lerp(weaponCam.fieldOfView, baseFOV * weapon.currentGunData.weaponFOV, Time.deltaTime * 8f);
                }
                else
                {
                    normalCam.fieldOfView = Mathf.Lerp(normalCam.fieldOfView, baseFOV, Time.deltaTime * 8f);
                    weaponCam.fieldOfView = Mathf.Lerp(weaponCam.fieldOfView, baseFOV, Time.deltaTime * 8f);
                }

                if (crouched)
                {
                    normalCam.transform.localPosition = Vector3.Lerp(normalCam.transform.localPosition, origin + Vector3.down * crouchAmount, Time.deltaTime * 6f);
                    weaponCam.transform.localPosition = Vector3.Lerp(weaponCam.transform.localPosition, origin + Vector3.down * crouchAmount, Time.deltaTime * 6f);
                }
                else
                {
                    normalCam.transform.localPosition = Vector3.Lerp(normalCam.transform.localPosition, origin, Time.deltaTime * 6f);
                    weaponCam.transform.localPosition = Vector3.Lerp(weaponCam.transform.localPosition, origin, Time.deltaTime * 6f);
                }
            }
        }

        #endregion

        #region Private Methods

        void RefreshMultiplayerState()
        {
            float cacheEulY = weaponParent.localEulerAngles.y;

            Quaternion targetRotation = Quaternion.identity * Quaternion.AngleAxis(aimAngle, Vector3.right);
            weaponParent.rotation = Quaternion.Slerp(weaponParent.rotation, targetRotation, Time.deltaTime * 8f);

            Vector3 finalRotation = weaponParent.localEulerAngles;
            finalRotation.y = cacheEulY;

            weaponParent.localEulerAngles = finalRotation;
        }

        void HeadBob(float _z, float _xIntensity, float _yIntensity)
        {
            float tAimAdjust = 1f;
            if (isAiming) tAimAdjust = 0.1f;
            targetWeaponBobPosition = weaponParentCurrentPos + new Vector3(Mathf.Cos(_z) * _xIntensity * tAimAdjust, Mathf.Sin(_z * 2f) * _yIntensity * tAimAdjust, 0);
        }

        void RefreshHealthBar()
        {
            float tHealthRatio = (float)currentHealth / (float)maxHealth;
            uiHealthBar.localScale = Vector3.Lerp(uiHealthBar.localScale, new Vector3(tHealthRatio, 1, 1), Time.deltaTime * 8f);
        }

        [PunRPC]
        void SetCrouch (bool _state)
        {
            if (crouched == _state) return;

            crouched = _state;

            if (crouched)
            {
                standingCollider.SetActive(false);
                crouchingCollider.SetActive(true);
                weaponParentCurrentPos += Vector3.down * crouchAmount;
            }
            else
            {
                standingCollider.SetActive(true);
                crouchingCollider.SetActive(false);
                weaponParentCurrentPos -= Vector3.down * crouchAmount;
            }
        }

        #endregion

        #region Public Methods

        public void TakeDamage(int _damage)
        {
            if (photonView.IsMine)
            {
                currentHealth -= _damage;
                RefreshHealthBar();

                if (currentHealth <= 0)
                {
                    //Die
                    manager.Spawn();
                    PhotonNetwork.Destroy(gameObject);
                }
            }            
        }

        #endregion
    }
}
