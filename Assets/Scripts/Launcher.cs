﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace Com.Chicutrino.FPSMultiplayerWelton
{
    public class Launcher : MonoBehaviourPunCallbacks
    {
        void Awake()
        {
            ///
            /// Whenever we're connected to the server and
            /// we're in a room it's gonna tell the room
            /// that we're in what scene we have loaded by
            /// itself without us having to tell it was having
            /// to manually update it
            ///

            PhotonNetwork.AutomaticallySyncScene = true;
            Connect();
        }

        public override void OnConnectedToMaster() // Esta función es llamada cuando la conexión ha sido establecida
        {
            Debug.Log("CONNECTED!");

            base.OnConnectedToMaster();
        }

        public override void OnJoinedRoom()
        {
            StartGame();

            base.OnJoinedRoom();
        }

        public override void OnJoinRandomFailed(short returnCode, string message) // Si falla la conexión, crea una sala
        {
            Create();

            base.OnJoinRandomFailed(returnCode, message);
        }

        public void Connect ()
        {
            Debug.Log("Trying to connect...");
            PhotonNetwork.GameVersion = "0.0.0";
            PhotonNetwork.ConnectUsingSettings();
        }

        public void Join ()
        {
            PhotonNetwork.JoinRandomRoom();
        }

        public void Create()
        {
            PhotonNetwork.CreateRoom("");
        }

        public void StartGame()
        {
            if(PhotonNetwork.CurrentRoom.PlayerCount == 1) // Si un player ya realizó la conexión carga la escena de la sala
            {
                PhotonNetwork.LoadLevel(1);
            }
        }
    }
}
