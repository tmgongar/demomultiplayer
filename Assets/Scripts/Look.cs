﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


namespace Com.Chicutrino.FPSMultiplayerWelton
{
    public class Look : MonoBehaviourPunCallbacks
    {
        #region Variables
        public static bool cursorLocked = true;

        public Transform player, normalCam, weaponCam, weapon;

        public float xSensitivity, ySensitivity, maxAngle;

        private Quaternion camCenter;
        #endregion

        #region Monobehavior Callbacks

        void Start()
        {
            camCenter = normalCam.localRotation; // set rotation origin for cameras to camCenter
        }

        void Update()
        {
            if (!photonView.IsMine) return;
            if (Pause.paused) return;

            SetY();
            SetX();

            UpdateCursorLock();

            weaponCam.rotation = normalCam.rotation;
        }
        #endregion

        #region Private Methods

        void SetY()
        {
            float tInput = Input.GetAxis("Mouse Y") * ySensitivity * Time.deltaTime;
            Quaternion tAdj = Quaternion.AngleAxis(tInput, -Vector3.right);
            Quaternion tDelta = normalCam.localRotation * tAdj;

            if(Quaternion.Angle(camCenter,tDelta) < maxAngle)
            {
                normalCam.localRotation = tDelta;
            }
            weapon.rotation = normalCam.rotation;
        }

        void SetX()
        {
            float tInput = Input.GetAxis("Mouse X") * xSensitivity * Time.deltaTime;
            Quaternion tAdj = Quaternion.AngleAxis(tInput, Vector3.up);
            Quaternion tDelta = player.localRotation * tAdj;

            player.localRotation = tDelta;
        }

        void UpdateCursorLock()
        {
            if (cursorLocked)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;

                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    cursorLocked = false;
                }
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;

                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    cursorLocked = true;
                }
            }
        }

        #endregion
    }
}
