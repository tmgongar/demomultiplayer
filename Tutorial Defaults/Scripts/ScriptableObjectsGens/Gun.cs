﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Gun", menuName = "Gun")]
public class Gun : ScriptableObject
{
    public string gunName;
    public int damage, ammo, burst, clipSize; //burst: 0 semi | 1 auto | 2+ burst fire
    public float fireRate, bloom, recoil, kickBack, aimSpeed, reload;
    public GameObject prefab;

    private int stash; //current ammo
    private int clip; //current clip

    public void Initialize()
    {
        stash = ammo;
        clip = clipSize;
    }

    public bool FireBullet()
    {
        if (clip > 0)
        {
            clip -= 1;
            return true;
        }
        else return false;
    }

    public void Reload()
    {
        stash += clip; //12 bullets
        clip = Mathf.Min(clipSize, stash);
        stash -= clip; //0 bullets - 12 bullets
    }

    public int GetStash() { return stash; }
    public int GetClip() { return clip; }
}
