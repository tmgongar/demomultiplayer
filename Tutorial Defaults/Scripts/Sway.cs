﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.Chicutrino.FPSMultiplayerWelton
{
    public class Sway : MonoBehaviour
    {
        #region Variables

        public float intensity, smooth;
        public bool isMine; //Sirve para arreglar la sincronización del recoil del arma ajena

        private Quaternion originRotation;

        #endregion

        #region Monobehaviour Callbacks

        private void Start()
        {
            originRotation = transform.localRotation;
        }

        private void Update()
        {
            UpdateSway();
        }

        #endregion

        #region Private Methods

        private void UpdateSway()
        {
            //Controls
            float tXMouse = Input.GetAxis("Mouse X");
            float tYMouse = Input.GetAxis("Mouse Y");

            if (!isMine)
            {
                tXMouse = 0;
                tYMouse = 0;
            }

            //Calculate target rotation
            Quaternion tXAdj = Quaternion.AngleAxis(-intensity * tXMouse, Vector3.up);
            Quaternion tYAdj = Quaternion.AngleAxis(intensity * tYMouse, Vector3.right);
            Quaternion targetRotation = originRotation * tXAdj * tYAdj;

            //Rotate towards target rotation
            transform.localRotation = Quaternion.Lerp(transform.localRotation, targetRotation, Time.deltaTime * smooth);
        }

        #endregion
    }
}
