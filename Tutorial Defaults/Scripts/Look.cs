﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


namespace Com.Chicutrino.FPSMultiplayerWelton
{
    public class Look : MonoBehaviourPunCallbacks
    {
        #region Variables
        public static bool cursorLocked = true;

        public Transform player, cams, weapon;

        public float xSensitivity, ySensitivity, maxAngle;

        private Quaternion camCenter;
        #endregion

        #region Monobehavior Callbacks
        void Start()
        {
            camCenter = cams.localRotation; // set rotation origin for cameras to camCenter
        }

        void Update()
        {
            if (!photonView.IsMine) return;

            SetY();
            SetX();

            UpdateCursorLock();
        }
        #endregion

        #region Private Methods

        void SetY()
        {
            float tInput = Input.GetAxis("Mouse Y") * ySensitivity * Time.deltaTime;
            Quaternion tAdj = Quaternion.AngleAxis(tInput, -Vector3.right);
            Quaternion tDelta = cams.localRotation * tAdj;

            if(Quaternion.Angle(camCenter,tDelta) < maxAngle)
            {
                cams.localRotation = tDelta;
            }
            weapon.rotation = cams.rotation;
        }

        void SetX()
        {
            float tInput = Input.GetAxis("Mouse X") * xSensitivity * Time.deltaTime;
            Quaternion tAdj = Quaternion.AngleAxis(tInput, Vector3.up);
            Quaternion tDelta = player.localRotation * tAdj;

            player.localRotation = tDelta;
        }

        void UpdateCursorLock()
        {
            if (cursorLocked)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;

                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    cursorLocked = false;
                }
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;

                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    cursorLocked = true;
                }
            }
        }

        #endregion
    }
}
