﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Photon.Pun;

namespace Com.Chicutrino.FPSMultiplayerWelton
{
    public class Weapon : MonoBehaviourPunCallbacks
    {
        #region Variables

        public Gun[] loadOut;
        public Transform weaponParent;
        public GameObject bulletHolePrefab;
        public LayerMask canBeShot;
        public bool isAiming = false;

        private float currentCooldown;
        private int currentIndex;
        private GameObject currentWeapon;

        private bool isReloading;

        #endregion

        #region Monobehaviour Callbacks

        private void Start()
        {
            foreach (Gun a in loadOut) a.Initialize();
            Equip(0);
        }

        void Update()
        {
            /// Enviamos información: RPC("Funcion", destinatario/s, parámetros[])
            if (photonView.IsMine && Input.GetKeyDown(KeyCode.Alpha1)) { photonView.RPC("Equip", RpcTarget.All, 0); } //En este caso decimos que nos hemos equipado
            if (photonView.IsMine && Input.GetKeyDown(KeyCode.Alpha2)) { photonView.RPC("Equip", RpcTarget.All, 1); }

            if(currentWeapon != null)
            {
                if (photonView.IsMine)
                {
                    Aim(Input.GetMouseButton(1));

                    if(loadOut[currentIndex].burst != 1)
                    {
                        if (Input.GetMouseButtonDown(0) && currentCooldown <= 0)
                        {
                            if (loadOut[currentIndex].FireBullet()) photonView.RPC("Shoot", RpcTarget.All); //Decimos a todos que hemos disparado
                            else StartCoroutine(Reload(loadOut[currentIndex].reload));
                        }
                    }
                    else
                    {
                        if (Input.GetMouseButton(0) && currentCooldown <= 0)
                        {
                            if (loadOut[currentIndex].FireBullet()) photonView.RPC("Shoot", RpcTarget.All); //Decimos a todos que hemos disparado
                            else StartCoroutine(Reload(loadOut[currentIndex].reload));
                        }
                    }

                    if(Input.GetKeyDown(KeyCode.R)) StartCoroutine(Reload(loadOut[currentIndex].reload));

                    //cooldown
                    if (currentCooldown > 0) currentCooldown -= Time.deltaTime;
                }

                //weapon position elasticity
                currentWeapon.transform.localPosition = Vector3.Lerp(currentWeapon.transform.localPosition, Vector3.zero, Time.deltaTime * 4f);
            }
        }

        #endregion

        #region Private Methods

        IEnumerator Reload(float _wait)
        {
            isReloading = true;
            currentWeapon.SetActive(false);

            yield return new WaitForSeconds(_wait);

            loadOut[currentIndex].Reload();
            currentWeapon.SetActive(true);

            isReloading = false;
        }

        [PunRPC]
        void Equip(int _ind)
        {
            if (currentWeapon != null)
            {
                if(isReloading) StopCoroutine("Reload");
                Destroy(currentWeapon);
            }
            currentIndex = _ind;

            GameObject tNewWeapon = Instantiate(loadOut[_ind].prefab, weaponParent.position, weaponParent.rotation, weaponParent) as GameObject;
            tNewWeapon.transform.localPosition = Vector3.zero;
            tNewWeapon.transform.localEulerAngles = Vector3.zero;
            tNewWeapon.GetComponent<Sway>().isMine = photonView.IsMine; //Sólo hará el efecto en el player local

            currentWeapon = tNewWeapon;
        }

        void Aim(bool _isAiming)
        {
            isAiming = _isAiming;
            Transform tAnchor = currentWeapon.transform.Find("Anchor");
            Transform tStateADS = currentWeapon.transform.Find("States/ADS");
            Transform tStateHip = currentWeapon.transform.Find("States/Hip");

            if (_isAiming)
            {
                //aim
                tAnchor.position = Vector3.Lerp(tAnchor.position, tStateADS.position, Time.deltaTime * loadOut[currentIndex].aimSpeed);
            }
            else
            {
                //hip
                tAnchor.position = Vector3.Lerp(tAnchor.position, tStateHip.position, Time.deltaTime * loadOut[currentIndex].aimSpeed);
            }
        }

        [PunRPC]
        void Shoot()
        {
            Transform tSpawn = transform.Find("Cameras/Normal Camera");

            //bloom
            Vector3 tBloom = tSpawn.position + tSpawn.forward * 1000f;
            tBloom += Random.Range(-loadOut[currentIndex].bloom, loadOut[currentIndex].bloom) * tSpawn.up;
            tBloom += Random.Range(-loadOut[currentIndex].bloom, loadOut[currentIndex].bloom) * tSpawn.right;
            tBloom -= tSpawn.position;
            tBloom.Normalize();

            //raycast
            RaycastHit tHit = new RaycastHit();
            if(Physics.Raycast(tSpawn.position, tBloom, out tHit, 1000f, canBeShot))
            {
                GameObject tNewHole = Instantiate(bulletHolePrefab, tHit.point + tHit.normal * 0.001f, Quaternion.identity) as GameObject;
                tNewHole.transform.LookAt(tHit.point + tHit.normal);
                Destroy(tNewHole, 5f);

                if (photonView.IsMine)
                {
                    //shooting other player on network and take damage
                    if(tHit.collider.gameObject.layer == 11)
                    {
                        //quita vida al player con el collider impactado
                        tHit.collider.transform.root.gameObject.GetPhotonView().RPC("TakeDamage", RpcTarget.All, loadOut[currentIndex].damage);
                    }
                }
            }

            //gun fx
            currentWeapon.transform.Rotate(-loadOut[currentIndex].recoil,0,0);
            currentWeapon.transform.position -= currentWeapon.transform.forward * loadOut[currentIndex].kickBack;

            //cooldown
            currentCooldown = loadOut[currentIndex].fireRate;
        }

        [PunRPC]
        private void TakeDamage(int _damage)
        {
            GetComponent<Player>().TakeDamage(_damage);
        }

        #endregion

        #region Public Methods

        public void RefreshAmmo(Text _text)
        {
            int tClip = loadOut[currentIndex].GetClip();
            int tStach = loadOut[currentIndex].GetStash();

            _text.text = tClip.ToString("D2") + " / " + tStach.ToString("D2");
        }

        #endregion
    }
}
